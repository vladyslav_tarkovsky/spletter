package spletterTest;

import io.appium.java_client.android.AndroidKeyCode;
import helper.DataProv;
import helper.Help;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class _04_GalleryTests extends Init {

	public static String Email = DataProv.prop("Email");
	public static String Password = DataProv.prop("Password");

	@Test(groups = "Compose")
	public void _01_T_ComposeSpletter() throws InterruptedException {

		splash.signInButton.click();
		signIn.signIn(Email, Password, size);
		Thread.sleep(1500);
		Help.swipe_left(driver);
		Assert.assertTrue(gallery.done.isDisplayed());
	}

	@Test(groups = "Compose")
	public void _02_T_ComposeSpletter() throws InterruptedException {

		Help.swipe_right(driver);
		Thread.sleep(1500);
		Assert.assertTrue(compose.gallery_icon.isDisplayed());
	}

	@Test(groups = "Compose")
	public void _03_T_ComposeSpletter() throws InterruptedException {

		compose.gallery_icon.click();
		Assert.assertTrue(gallery.done.isDisplayed());
	}

	@Test(groups = "Compose")
	public void _04_T_ComposeSpletter() throws InterruptedException {

		driver.sendKeyEvent(AndroidKeyCode.BACK);
		Assert.assertTrue(compose.gallery_icon.isDisplayed());
	}

	@Test(groups = "Compose")
	public void _05_T_ComposeSpletter() throws InterruptedException {

		compose.gallery_icon.click();
		gallery.gallery_title.click();

		for (WebElement el : gallery.gallery_titles) {
			String tl = el.getText();
			el.click();
			Thread.sleep(500);
			System.out.println("sdd - " + gallery.gallery_title.getText());
			System.out.println("qty el - " + gallery.pictures_list.size());
			Assert.assertEquals(gallery.gallery_title.getText(), tl);
			gallery.gallery_title.click();
		}

	}

	public int selPicQty;

	@Test(groups = "Compose")
	public void _06_T_ComposeSpletter() throws InterruptedException {

		gallery.gallery_title.click();

		for (int i = 1; i < gallery.pictures_list.size(); i++) {
			gallery.pictures_list.get(i).click();
		}
		selPicQty = Integer.parseInt(Help.splitStr(
				gallery.selected_photos.getText(), " ")[0]);
		Assert.assertEquals(selPicQty, gallery.pictures_list.size() - 1);

	}

	@Test(groups = "Compose")
	public void _07_T_ComposeSpletter() throws InterruptedException {

		gallery.gallery_title.click();
		gallery.gallery_titles.get(1).click();
		gallery.pictures_list.get(1).click();
		int selPicQtyNext = Integer.parseInt(Help.splitStr(
				gallery.selected_photos.getText(), " ")[0]);
		Assert.assertEquals(selPicQty + 1, selPicQtyNext);

	}

	@Test(groups = "Compose")
	public void _08_T_ComposeSpletter() throws InterruptedException {

		gallery.clear_selection.click();
		Assert.assertFalse(Help.isElementPresent(gallery.clear_selection));
	}
	
	@Test(groups = "Compose")
	public void _09_T_ComposeSpletter() throws InterruptedException {

		for (int i = 1; i < gallery.pictures_list.size(); i++) {
			gallery.pictures_list.get(i).click();
		}
		gallery.done.click();
		Thread.sleep(3000);
	}

}
