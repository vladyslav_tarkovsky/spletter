package spletterTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import helper.DataProv;
import helper.Help;

public class _03_ComposeSpletter extends Init{
	
	public static String Email = DataProv.prop("Email");
	public static String Password = DataProv.prop("Password");
	public static String text = DataProv.prop("testText_01");
	
	@Test(groups = "Compose")
	public void _01_T_ComposeSpletter() throws InterruptedException {
		
		splash.signInButton.click();
		signIn.signIn(Email, Password, size);
		Assert.assertTrue(compose.gallery_icon.isDisplayed());
	}
	
	@Test(groups = "Compose")
	public void _02_T_ComposeSpletter() throws InterruptedException {
		
		Help.swipe_right(driver);
		Assert.assertTrue(profile.settings_button.isDisplayed());
	}
	
	@Test(groups = "Compose")
	public void _03_T_ComposeSpletter() throws InterruptedException {
		
		Help.swipe_left(driver);
		Assert.assertTrue(compose.gallery_icon.isDisplayed());
	}
	
	@Test(groups = "Compose")
	public void _04_T_ComposeSpletter() throws InterruptedException {
		
		Help.swipe_left(driver);
		Thread.sleep(1500);
		Assert.assertTrue(gallery.done.isDisplayed());
	}
	
	@Test(groups = "Compose")
	public void _05_T_ComposeSpletter() throws InterruptedException {
		
		Help.swipe_right(driver);
		compose.text_fields.sendKeys(text);
		Help.swipe_right(driver);
		Thread.sleep(1500);
		Help.swipe_left(driver);
		Assert.assertEquals(compose.text_fields.getText(), text);
	}
	
	


}
