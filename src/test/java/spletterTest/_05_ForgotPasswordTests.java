package spletterTest;

import helper.DataProv;
import helper.Help;

import org.testng.Assert;
import org.testng.annotations.Test;

public class _05_ForgotPasswordTests extends Init {

	public static String Email = DataProv.prop("Email");

	@Test(groups = "ForgotPassword")
	public void _01_T_ForgotPassword() throws InterruptedException {

		splash.signInButton.click();
		signIn.forgot_password.click();
		Assert.assertEquals(forgotPassword.fp_title.getText(),
				"Forgot Password");
	}

	@Test(groups = "ForgotPassword")
	public void _02_T_ForgotPassword() throws InterruptedException {

		Assert.assertEquals(forgotPassword.fp_text.getText(),
				"Please enter the email address associated with your Spletter account:");

	}

	@Test(groups = "ForgotPassword")
	public void _03_T_ForgotPassword() throws InterruptedException {

		Assert.assertEquals(forgotPassword.email_address_edittext.getText(),
				"Your Email Address");

	}

	@Test(groups = "ForgotPassword")
	public void _04_T_ForgotPassword() throws InterruptedException {

		forgotPassword.email_address_edittext.click();
		Help.tapOnDone(driver, size);

	}

	@Test(groups = "ForgotPassword")
	public void _05_T_ForgotPassword() throws InterruptedException {

		forgotPassword.okButton.click();
		forgotPassword.email_address_edittext.sendKeys("aa");
		Help.tapOnDone(driver, size);
		// Assert.assertEquals(forgotPassword.email_address_edittext.getText(),
		// "Your Email Address");
	}

	@Test(groups = "ForgotPassword")
	public void _06_T_ForgotPassword() throws InterruptedException {

		forgotPassword.okButton.click();
		forgotPassword.email_address_edittext.sendKeys("dfgd@gmail.com");
		Help.tapOnDone(driver, size);
		// Assert.assertEquals(forgotPassword.email_address_edittext.getText(),
		// "Your Email Address");
	}

	@Test(groups = "ForgotPassword")
	public void _07_T_ForgotPassword() throws InterruptedException {

		forgotPassword.okButton.click();
		forgotPassword.email_address_edittext.sendKeys(Email);
		Help.tapOnDone(driver, size);
		Assert.assertEquals(forgotPassword.alertTitle.getText(), "Email Sent");
	}

}
