package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GalleryPage {
	
private static WebDriver driver;
	
	public GalleryPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "android:id/text1") 
	public WebElement gallery_title;
	
	@FindBy(name = "Done") 
	public WebElement done;
	
	@FindBy(name = "Take a Picture") 
	public WebElement takePicture;
	
	@FindBy(xpath = "//android.widget.GridView/android.widget.RelativeLayout") 
	public List<WebElement> pictures_list;
	
	@FindBy(id = "android:id/text1") 
	public List<WebElement> gallery_titles;
	
	@FindBy(id = "com.spletter.spletter:id/selected_photos") 
	public WebElement selected_photos;
	
	@FindBy(id = "com.spletter.spletter:id/clear_selection") 
	public WebElement clear_selection;
	
	
	
//	public  List<WebElement> gallery_titles_list() { 
//		return driver.findElements(By.id("android:id/text1"));  
//	}
	


}
