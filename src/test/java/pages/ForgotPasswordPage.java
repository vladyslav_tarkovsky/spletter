package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForgotPasswordPage {
	
	private static WebDriver driver;
	
	public ForgotPasswordPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//android.widget.TextView[1]") 
	public WebElement fp_title;
	
	@FindBy(xpath = "//android.widget.TextView[2]") 
	public WebElement fp_text;
	
	@FindBy(id = "com.spletter.spletter:id/email_address_edittext") 
	public WebElement email_address_edittext;
	
//	-----------------------------------------------------
	
	@FindBy(id = "android:id/button1") 
	public WebElement okButton;
	
	@FindBy(id = "android:id/alertTitle") 
	public WebElement alertTitle;
	

}
