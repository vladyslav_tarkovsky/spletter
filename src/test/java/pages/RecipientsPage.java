package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RecipientsPage {
	
private static WebDriver driver;
	
	public RecipientsPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//android.widget.FrameLayout/android.widget.ImageButton") 
	public WebElement back_button;
	
	@FindBy(name = "Recipients") 
	public WebElement title_view;
	
	@FindBy(id = "com.spletter.spletter:id/action_done") 
	public WebElement action_done;
	
	@FindBy(id = "com.spletter.spletter:id/search_edittext") 
	public WebElement search_edittext;
	
	@FindBy(id = "com.spletter.spletter:id/item_layout") 
	public List<WebElement> item_layout;
	
	@FindBy(id = "com.spletter.spletter:id/name_text") 
	public List<WebElement> search_name;
	
	@FindBy(id = "com.spletter.spletter:id/address_type_text") 
	public List<WebElement> address_type_text;
	
	@FindBy(id = "com.spletter.spletter:id/address_text") 
	public List<WebElement> address_text;
	

}
