package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {
	
private static WebDriver driver;
	
	public ProfilePage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "com.spletter.spletter:id/settings_button") 
	public WebElement settings_button;
	
	@FindBy(id = "com.spletter.spletter:id/name_text") 
	public WebElement name_text;
	
	@FindBy(name = "My Spletters") 
	public WebElement my_spletters_button;
	
	@FindBy(name = "Earn Spletter Dollars") 
	public WebElement earn_spletters_dollars_button;
	
	@FindBy(name = "Contacts") 
	public WebElement Contacts;
	
	@FindBy(name = "Distribution Groups") 
	public WebElement distribution_groups_button;

}
